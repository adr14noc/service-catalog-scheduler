package br.com.itau.scheduler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ComponentScan("br.com.itau.*")
public class App {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(App.class);
    }
}
