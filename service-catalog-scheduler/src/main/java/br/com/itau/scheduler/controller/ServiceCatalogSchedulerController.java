package br.com.itau.scheduler.controller;

import java.io.IOException;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.scheduler.implementation.UpdateProjectImp;

@RestController
@RequestMapping("/scheduler")
public class ServiceCatalogSchedulerController {
	
	
	
	@RequestMapping(path="/group/{id}", method=RequestMethod.POST)
	public ResponseEntity<?> updateGroupProjectsById(@PathVariable Long id) throws IOException{
		
		UpdateProjectImp update = new UpdateProjectImp();
		update.updateAllGroupProjects();
		
		
		return ResponseEntity.ok("");
	}


@RequestMapping(path="/group/path/{path}", method=RequestMethod.POST)
public ResponseEntity<?> updateGroupProjectsByPath(@PathVariable String path) throws IOException{
	try {
		
		//TODO IMPLEMENTAR UPDATE
		
		return ResponseEntity.ok("");
		
	} catch (Exception e) {
		return ResponseEntity.badRequest().body(e.getMessage());	
	}
}



@RequestMapping(path="/project/{id}", method=RequestMethod.POST)
public ResponseEntity<?> updateProjectsById(@PathVariable Integer id) throws IOException{
	
	try {
		
		//TODO IMPLEMENTAR UPDATE
		
		return ResponseEntity.ok("");
		
	} catch (Exception e) {
		return ResponseEntity.badRequest().body(e.getMessage());	
	}
}

@RequestMapping(path="/project/path/{path}", method=RequestMethod.POST)
public ResponseEntity<?> updateProjectsByPath(@PathVariable String path) throws IOException{
	
	
	try {
		
		//TODO IMPLEMENTAR UPDATE
		
		return ResponseEntity.ok("");
		
	} catch (Exception e) {
		return ResponseEntity.badRequest().body(e.getMessage());	
	}
	
}


}

