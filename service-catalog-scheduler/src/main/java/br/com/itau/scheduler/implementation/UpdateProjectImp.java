package br.com.itau.scheduler.implementation;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.gitlab.api.GitlabAPI;
import org.gitlab.api.models.GitlabProject;
import org.springframework.stereotype.Component;

import br.com.itau.scheduler.model.GitLabGroupConfig;
import br.com.itau.scheduler.service.GitLabGroupConfigService;

@Component
public class UpdateProjectImp {


	GitLabGroupConfigService gitLabConfigService = new GitLabGroupConfigService();

	private GitLabGroupConfig gitLabConfig;


	public void updateAllGroupProjects() throws IOException {
		
		gitLabConfig = gitLabConfigService.getDefaultConfig();
		
		GitlabAPI gitLabApi = GitlabAPI.connect(gitLabConfig.getGitLabBaseUrl(),gitLabConfig.getGitLabToken().getToken());
		
		System.out.println("URL: " + gitLabConfig.getGitLabBaseUrl());
		
		List<GitlabProject> gitLabProjects = gitLabApi.getGroupProjects(gitLabConfig.getGitLabGroupId());
		
		
		
		for (GitlabProject gitlabProject : gitLabProjects) {
			
			System.out.println("Nome Projeto: " + gitlabProject.getName());
			System.out.println("Ultima atividade : " + gitlabProject.getLastActivityAt());
			System.out.println("Project id : " + gitlabProject.getId());
						
			try {
			System.out.println(new String(gitLabApi.getRawFileContent(gitlabProject, "master", ".gitignore"))); 
			System.out.println(new String(gitLabApi.getRawFileContent(gitlabProject, "master", "pom.xml")));
			}
			
			catch (Exception e) {
				// TODO: handle exception
			}
			
			
			
			
		}
		
		
		//TODO Bug de cast linkedHashMap to object
		
//		gitLabConfigList = gitLabConfigService.getListGroupConfig();
//			
//
//		//TODO ver bug aqui
//		for (GitLabGroupConfig gitLabGroupConfig : gitLabConfigList) {
//			 GitlabAPI gitLabApi =
//					 GitlabAPI.connect(gitLabGroupConfig.getGitLabBaseUrl(),gitLabGroupConfig.getGitLabToken().getToken());
//			 
//			 System.out.println(gitLabGroupConfig.getGitLabBaseUrl());
//			
//			
//		}
		
	}

}
