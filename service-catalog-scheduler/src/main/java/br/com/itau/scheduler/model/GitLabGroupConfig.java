package br.com.itau.scheduler.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@Entity
public class GitLabGroupConfig {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@NotNull
	private String 			gitLabBaseUrl;
	private Integer 		gitLabGroupId;
	private String  		gitLabGroupPath;
	private String          cronScheduler;
	
	
	@OneToOne(cascade =  CascadeType.ALL)
	private GitLabToken 	gitLabToken;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getGitLabBaseUrl() {
		return gitLabBaseUrl;
	}
	public void setGitLabBaseUrl(String gitLabBaseUrl) {
		this.gitLabBaseUrl = gitLabBaseUrl;
	}
	public Integer getGitLabGroupId() {
		return gitLabGroupId;
	}
	public void setGitLabGroupId(Integer gitLabGroupId) {
		this.gitLabGroupId = gitLabGroupId;
	}
	public String getGitLabGroupPath() {
		return gitLabGroupPath;
	}
	public void setGitLabGroupPath(String gitLabGroupPath) {
		this.gitLabGroupPath = gitLabGroupPath;
	}
	public GitLabToken getGitLabToken() {
		return gitLabToken;
	}
	public void setGitLabToken(GitLabToken gitLabToken) {
		this.gitLabToken = gitLabToken;
	}
	public String getCronScheduler() {
		return cronScheduler;
	}
	public void setCronScheduler(String cronScheduler) {
		this.cronScheduler = cronScheduler;
	}
	
	
	public String toString() {
		
		  StringBuilder retorno = null;
		  
		  	retorno.append(id + "\n");		  
		    retorno.append(gitLabBaseUrl + "\n");
		    retorno.append(gitLabGroupId + "\n");
		    retorno.append(gitLabGroupPath + "\n");
		    retorno.append(cronScheduler + "\n");
		    
		return retorno.toString() ;
		
		
	}
	

}