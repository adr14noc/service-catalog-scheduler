package br.com.itau.scheduler.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import br.com.itau.scheduler.model.GitLabGroupConfig;
import br.com.itau.scheduler.model.GitLabToken;

@Component
public class GitLabGroupConfigService {
	
	
	private static RestTemplate rest =  new RestTemplate();
	private static final Logger log = LoggerFactory.getLogger("GitLabGroupConfigService");
	
//	@Value("${gitlab.config.url}")
	String urlConfigService="http://localhost:8082/config";
	
	@Value("${gitlab.default.baseUrl}")
	private String gitLabDefaulBaseUrl;
	
	   	
	@Value("${gitlab.default.groupID}")
	private String gitLabDefaulGroupID;
	
	@Value("${gitlab.default.groupPath}")
	private String gitLabDefaulGroupPath;
	
	@Value("${gitlab.default.token}")
	private String gitLabDefaulttoken;
	
	

	public GitLabGroupConfig getDefaultConfig() {
		log.info("Buscando config Default");
		log.info("URL " +urlConfigService);
		
		try {
			return rest.getForObject(urlConfigService +"/1", GitLabGroupConfig.class);
			
		} catch (Exception e) {
			log.error("Erro ao chamar serviço de configuração: " +e.getMessage());
			log.info("Utilizando valores de configuração default ");
			
			GitLabGroupConfig gitLabGroupConfig = new GitLabGroupConfig();
			gitLabGroupConfig.setGitLabBaseUrl(gitLabDefaulBaseUrl);
			gitLabGroupConfig.setGitLabGroupId(Integer.valueOf(gitLabDefaulGroupID));
			gitLabGroupConfig.setGitLabGroupPath(gitLabDefaulGroupPath);
			
			GitLabToken gitLabToken = new GitLabToken();
			
			gitLabToken.setToken(gitLabDefaulttoken);
			gitLabGroupConfig.setGitLabToken(gitLabToken);
			
			return gitLabGroupConfig;
			
		}
	}
		public List<GitLabGroupConfig> getListGroupConfig() {
			log.info("Buscando config Default");
			log.info("URL " +urlConfigService);
			
			try {
				
				
				List<GitLabGroupConfig> gitLabConfig = rest.getForObject(urlConfigService, List.class);
				return gitLabConfig;
				
			} catch (Exception e) {
				log.error("Erro ao chamar serviço de configuração: " +e.getMessage());
				
				return null;
				
			}
		
	}

	
}
