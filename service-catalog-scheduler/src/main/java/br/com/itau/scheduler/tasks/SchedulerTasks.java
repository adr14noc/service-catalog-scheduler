package br.com.itau.scheduler.tasks;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class SchedulerTasks {
	
	 private static final Logger log = LoggerFactory.getLogger(SchedulerTasks.class);

	    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	    
		
		
	    //todos os dias a 1h da manha
	    @Scheduled(cron="0 0 1 1/1 * ?")
	    public void updateProjects() {
	    	
	        log.info("The time is now {}", dateFormat.format(new Date()));
	        
	        //TODO Implementar logica de atualizar repositorios
	        
	        
	    }
	}
